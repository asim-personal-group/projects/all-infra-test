const csvToArray = (data) => {
	try {
		const csvRows = data.split("\n");
		const output = csvRows.map((row) => row.split(","));
		for (const row of output) {
			if (row.length !== 3 || typeof row[0] !== "string" || isNaN(row[1]) || isNaN(row[2])) throw new Error("Invalid Data Received");
		}
		return output;
	} catch (err) {
		throw err;
	}
};

const csvToObject = (data, headers) => {
	try {
		let output = [];
		let dataArray = csvToArray(data);
		if (dataArray.length > 0) {
			if (dataArray[0].length !== headers.length) {
				throw new Error("Headers do not match data");
			} else {
				for (const coffeeShop of dataArray) {
					let shop = {};
					for (const [index, key] of headers.entries()) {
						shop[key] = coffeeShop[index];
					}
					output.push(shop);
				}
			}
		}
		return output;
	} catch (err) {
		throw err;
	}
};

module.exports = {
	csvToArray,
	csvToObject
};
