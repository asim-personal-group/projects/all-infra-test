const axios = require("axios");
const constants = require("../config/constants");
const { coffeeShopsTransformer } = require("../transformers");
const headers = ["shop", "y", "x"];

const getAllCoffeeShopsByFileName = async (file) => {
	try {
		let url = `${constants.coffeeShops.baseUrl}/${file}?token=${constants.coffeeShops.token}`;
		let response = await axios.get(url);
		return coffeeShopsTransformer.csvToObject(response.data, headers);
	} catch (err) {
		if (err.response.status === 404) throw new Error("File not found");
		else throw err;
	}
};

const getAllCoffeeShopsByUrl = async (url) => {
	try {
		let regex = new RegExp(constants.regex.url);
		if (!url.match(regex)) throw new Error("Invalid URL");
		let response = await axios.get(url);
		return coffeeShopsTransformer.csvToObject(response.data, headers);
	} catch (err) {
		if (err?.response?.status === 404) throw new Error("File not found");
		else throw err;
	}
};

module.exports = {
	getAllCoffeeShopsByFileName,
	getAllCoffeeShopsByUrl
};
