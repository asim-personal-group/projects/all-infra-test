const { coffeeShopsService } = require("./services");
const { calculateDistance } = require("./utils");

main = async () => {
	try {
		const [x1, y1, fileName] = process.argv.slice(2, process.argv.length);
		if (!x1 || !y1 || !fileName || isNaN(x1) || isNaN(y1)) throw new Error("Invalid Parameters");
		const csd = await coffeeShopsService.getAllCoffeeShopsByUrl(fileName);
		for (const shop of csd) {
			const { x: x2, y: y2 } = shop;
			shop.distance = calculateDistance({ x1, y1, x2, y2 });
		}
		let output = csd
			.sort((a, b) => (a.distance > b.distance ? 1 : -1))
			.slice(0, 3)
			.map((cs) => `${cs.shop},${parseFloat(cs.distance.toFixed(4))}`)
			.join("\n");
		console.log(output);
	} catch (err) {
		console.error(err.message);
	}
};

main();
