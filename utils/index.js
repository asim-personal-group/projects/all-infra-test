const calculateDistance = ({ x1, y1, x2, y2 }) => {
	try {
		if (isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2)) throw new Error("Coordinates must be numbers");
		else {
			let distance = Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));
			return distance;
		}
	} catch (err) {
		throw err;
	}
};

module.exports = { calculateDistance };
