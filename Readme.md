## Initial Setup

At the root of the project folder, please execute the following command:

`npm install`

## Running the Project

To run the project, enter the following command at the root of the project and replace X_CORD, Y_CORD and FILE_URL with the desired values:

`node ./ X_CORD Y_CORD FILE_URL`

## NOTE

The distance values returned by this script do not match the values given in the [Example](#example) section. This is because according to the instructions in the [Input](#input) section, the second column in the csv contains y coordinates and the third column in the file contains x coordinates. The distance values given in the example can be achieved if the second column is assumed to contain the x coordinates and the third column is assumed to contain the y coordinates or by switching the x and y coordinates in the input arguments.

## Overview

You have been hired by a company that builds a mobile app for coffee addicts. You are
responsible for taking the user’s location and returning a list of the three closest coffee shops.

## <a id="input"></a>Input

The coffee shop list is a comma separated file with rows of the following form:
`Name,Y Coordinate,X Coordinate`

The quality of data in this list of coffee shops may vary. Malformed entries should cause the
program to exit appropriately.

Your program will be executed directly from the command line and will be provided three
arguments in the following order:
`<user x coordinate> <user y coordinate> <shop data url>`

Notice that the data file will be read from an network location (ex: https://raw.githubusercontent.com/allinfra-ltd/test_oop/master/coffee_shops.csv?token=AA5HVELWEJRKDGHELZ6232DAOHE54)

## Output

Write a program that takes the user’s coordinates encoded as listed above and prints out a
newline­separated list of the three closest coffee shops (including distance from the user) in
order of closest to farthest. These distances should be rounded to four decimal places.

Assume all coordinates lie on a plane.

## <a id="example"></a>Example

Using the [coffee_shops.csv](coffee_shops.csv)

**Input**

`47.6 -122.4 coffee_shops.csv`

**Expected output**

```
Starbucks Seattle2,0.0645
Starbucks Seattle,0.0861
Starbucks SF,10.0793
```
